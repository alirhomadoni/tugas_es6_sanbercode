// soal No 1: mengubah fungsi menjadi fungsi arrow
// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }
const golden = ()=> console.log("this is golden!!")

golden()

// Soal No. 2 : Mengubah menjadi bentuk yang lebih sederhana

const newFunction = (firstName, lastName)=>{
  return {
    fullName(){
      console.log(`${firstName} ${lastName}`)
    }
  }
}

//Driver Code
newFunction("William", "Imoh").fullName()

// Soal No: 3 : Destructuring

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
let {firstName,lastName,destination,occupation,spell}=newObject

// Driver code
console.log(firstName, lastName, destination, occupation, spell)



// Soal No. 4 : Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)

// soal No. 5 :

const planet = "earth"
const view = "glass"
var before = `Lorem ${view}'dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
   // Driver Code
console.log(before)
